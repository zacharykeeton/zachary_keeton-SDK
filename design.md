# Design

I'm very much a fan of the classic "layered" or "n-tier" architecture pattern. As such, I've used it here.

## Layer 0: The "DataService"

The `DataService` (`src/data/service/index.ts`) does the raw fetching across the wire. It wrps `node-fetch`, converts responses to JSON, and can throw custom `FetchError` when needed. Wrapping the 3rd-party dependency allows us to more easily switch data sources if needed (e.g. away from the API and to a directly accessible DB). We would have room within the wraper to shim in any needed adapter logic.

## Layer 1: Various Entity Services

On the next layer up, We have services for each API entity.  
See:

- src/book/service/index.ts
- src/chapter/service/index.ts
- src/character/service/index.ts
- src/movie/service/index.ts
- src/quote/service/index.ts

Each of these have their own methods and return instances of their own custom classes (e.g. instances of `Movie` (`src/movie/models/Movie.ts`)). These services make up the bulk of the external API of the SDK.
These services further decouple the SDK from the dataSource, (e.g. it allows us to use alternate data sources, combine multiple data sources, or to add other logic).

## Layer 2 (top-layer): The Client

The top-level, exposed client instantiates each service as a property. This modularity enables the parallelization of development work on the separate services.

## Bonus: Dependency Injection support

The top-level client has a `DataService` parameter in the constructor constructor function that is then fed down to all the child services. This allows for easier mocking during testing, or even in-field re-implementaton of the dataLayer without needing to re-release the entire SDK.

These classic patterns have survived this long for a reason: they're great.

## Areas for Code Improvement

- Code could be DRYer by leveraging generics
- Auto documentation generation with `typedoc` is incomplete (see: npm run gen_docs). It generates into the `docs` directory, but needs more attention (code comments => docs, etc.)
- Adding configurable retry logic to the dataService will make it more robust
- The querystring generator functions around pagination, sorting, and filtering could be refactored to be more elegant. Although they are passing their unit tests :)
- Add traversal functionality to the Classes (e.g Character.getQuotes()) to add more elegance to the API and improve developer experience.
- Add webpack to minify the deployed package for browser usage
