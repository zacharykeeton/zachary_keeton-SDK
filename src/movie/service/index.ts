// https://the-one-api.dev/documentation

import DataService from "../../data/service";
// import Book from "./Book";
// import Chapter from "./Chapter";
// import { ChapterDoc, BookDoc } from "./interfaces";

import MovieDoc from "../models/MovieDoc";
import Movie from "../models/Movie";
import FindConfig from "../../data/models/FindConfig";
import ServiceResult from "../../models/ServiceResult";

export default class MovieService {
  constructor(public requestService = new DataService()) {}

  public async find(options?: FindConfig): Promise<ServiceResult> {
    const data = await this.requestService.fetchJSON(
      "https://the-one-api.dev/v2/movie",
      options
    );

    return {
      data: data.docs.map(
        (doc: MovieDoc) => new Movie(doc, this.requestService)
      ),
      metadata: {
        total: data.total,
        limit: data.limit,
        offset: data.offset,
        page: data.page,
        pages: data.pages,
      },
    };
  }

  public async findOne(movieId: string) {
    const data = await this.requestService.fetchJSON(
      `https://the-one-api.dev/v2/movie/${movieId}`
    );
    return new Movie(data.docs[0], this.requestService);
  }
}
