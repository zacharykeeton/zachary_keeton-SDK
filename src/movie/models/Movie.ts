import MovieDoc from "../models/MovieDoc";
import DataService from "../../data/service";

export default class Movie {
  _id: string;
  name: string;
  runtimeInMinutes: number;
  budgetInMillions: number;
  boxOfficeRevenueInMillions: number;
  academyAwardNominations: number;
  academyAwardWins: number;
  rottenTomatoesScore: number;

  constructor(doc: MovieDoc, private requestService: DataService) {
    this._id = doc._id;
    this.name = doc.name;
    this.runtimeInMinutes = doc.runtimeInMinutes;
    this.budgetInMillions = doc.budgetInMillions;
    this.boxOfficeRevenueInMillions = doc.boxOfficeRevenueInMillions;
    this.academyAwardNominations = doc.academyAwardNominations;
    this.academyAwardWins = doc.academyAwardWins;
    this.rottenTomatoesScore = doc.rottenTomatoesScore;
  }
}
