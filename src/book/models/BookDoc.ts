import { Doc } from "../../models/Doc";

export interface BookDoc extends Doc {
  readonly _id: string;
  readonly name: string;
}
