import DataService from "../../data/service";
import Chapter from "../../chapter/models/Chapter";
import ChapterDoc from "../../chapter/models/ChapterDoc";
import ChapterService from "../../chapter/service";

export default class Book {
  private readonly chapterService: ChapterService;
  constructor(
    public readonly _id: string,
    public readonly name: string,
    private readonly dataService: DataService
  ) {
    this.chapterService = new ChapterService(this.dataService);
  }
  public async getChapters() {
    const response = await this.chapterService.findByBook(this._id);
    return response.data;
  }
}
