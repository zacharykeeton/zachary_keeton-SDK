// https://the-one-api.dev/documentation

import DataService from "../../data/service";
import Book from "../../book/models/Book";
import { BookDoc } from "../models/BookDoc";
import FindConfig from "../../data/models/FindConfig";
import ServiceResult from "../../models/ServiceResult";

interface Service {
  find: (options?: FindConfig) => Promise<ServiceResult>;
}

/**
 * The BookService Class
 */
export default class BookService implements Service {
  private readonly baseUrl = "https://the-one-api.dev/v2/book";
  constructor(private readonly requestService = new DataService()) {}

  public async find(options?: FindConfig): Promise<ServiceResult> {
    const data = await this.requestService.fetchJSON(this.baseUrl, options);

    return {
      data: data.docs.map(
        (doc: BookDoc) => new Book(doc._id, doc.name, this.requestService)
      ),
      metadata: {
        total: data.total,
        limit: data.limit,
        offset: data.offset,
        page: data.page,
        pages: data.pages,
      },
    };
  }

  public async findOne(bookId: string) {
    const data = await this.requestService.fetchJSON(
      `${this.baseUrl}/${bookId}`
    );
    const { _id, name } = data.docs[0];
    return new Book(_id, name, this.requestService);
  }
}
