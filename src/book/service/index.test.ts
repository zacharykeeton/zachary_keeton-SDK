import * as dotenv from "dotenv";
dotenv.config();
import BookService from "./index";
import DataService from "../../data/service";

describe("BookService", () => {
  it("exists", () => {
    expect(BookService).toBeDefined();
  });

  describe("find", () => {
    it.only("works with valid key", async () => {
      const validKey = process.env.API_KEY;
      const requestService = new DataService(validKey);
      const bookService = new BookService(requestService);
      const result = await bookService.find();
      console.log(result);
    });
  });

  describe("findOne", () => {});
});
