export default interface ServiceResult {
  data: any[];
  metadata: {
    total: number;
    limit: number;
    offset: number;
    page: number;
    pages: number;
  };
}
