import DataService from "../data/service";

export default interface OneAPIClientConfig {
  requestService?: DataService;
  apiKey?: string;
}
