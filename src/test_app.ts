import { OneAPIClient } from ".";
import * as dotenv from "dotenv";
dotenv.config();

(async () => {
  const client = new OneAPIClient({ apiKey: process.env.API_KEY });

  //   const quotes = await client.quotes.find({ pagination: { limit: 1 } });
  //   const firstQuote = quotes.data[0];

  //   console.log(await firstQuote.getMovie());
  //   console.log(await firstQuote.getCharacter());

  const chapters = await client.chapters.find({ pagination: { limit: 1 } });
  const firstChapter = chapters.data[0];

  console.log(await firstChapter.getBook());
})();
