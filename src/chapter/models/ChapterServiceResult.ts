import ServiceResult from "../../models/ServiceResult";
import Chapter from "./Chapter";

export default interface ChapterServiceResult extends ServiceResult {
  data: Chapter[];
}
