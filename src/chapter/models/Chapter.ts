import BookService from "../../book/service";
import ChapterDoc from "./ChapterDoc";
import DataService from "../../data/service";
import Book from "../../book/models/Book";

export default class Chapter {
  _id: string;
  name: string;
  bookId: string;
  private fullBook?: Book;

  private bookService: BookService;
  constructor(
    { _id, chapterName, book }: ChapterDoc,
    private requestService: DataService
  ) {
    this._id = _id;
    this.name = chapterName;
    this.bookId = book;
    this.bookService = new BookService(this.requestService);
  }

  getBook() {
    if (this.fullBook) {
      return Promise.resolve(this.fullBook);
    } else {
      return this.bookService.findOne(this.bookId);
    }
  }
}
