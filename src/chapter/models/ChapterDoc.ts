import { Doc } from "../../models/Doc";

export default interface ChapterDoc extends Doc {
  _id: string;
  chapterName: string;
  book: string;
}
