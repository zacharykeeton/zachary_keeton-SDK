/// <reference types="jest" />
import * as dotenv from "dotenv";
dotenv.config();
import DataService from "../../data/service";

import ChapterService from "./index";

describe("The ChapterService", () => {
  it("exists", () => {
    expect(ChapterService).toBeDefined();
  });

  describe(".find", () => {
    it("exists", () => {
      const chapterService = new ChapterService();
      expect(chapterService.find).toBeDefined();
    });

    it.only("works with valid key", async () => {
      const validKey = process.env.API_KEY;
      const requestService = new DataService(validKey);
      const chapterService = new ChapterService(requestService);
      const result = await chapterService.find();
      console.log(result);

      // expect(chapterService.find).toBeDefined();
    });
  });
});
