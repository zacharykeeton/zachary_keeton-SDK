// QUOTE CLASS SHOULD BE ABLE TO GET MOVIES

// https://the-one-api.dev/documentation

import MovieService from "../../movie/service";
import DataService from "../../data/service";
import Chapter from "../models/Chapter";
import ChapterDoc from "../models/ChapterDoc";
import FindConfig from "../../data/models/FindConfig";
import ChapterServiceResult from "../models/ChapterServiceResult";

/**
 * Chapter Service
 */
export default class ChapterService {
  private baseURL = "https://the-one-api.dev/v2";
  constructor(private dataService = new DataService()) {}

  public async find(options?: FindConfig): Promise<ChapterServiceResult> {
    const data = await this.dataService.fetchJSON(
      `${this.baseURL}/chapter`,
      options
    );
    return {
      data: data.docs.map(
        (doc: ChapterDoc) => new Chapter(doc, this.dataService)
      ),
      metadata: {
        total: data.total,
        limit: data.limit,
        offset: data.offset,
        page: data.page,
        pages: data.pages,
      },
    };
  }

  public async findOne(chapterId: string) {
    const data = await this.dataService.fetchJSON(
      `${this.baseURL}/chapter/${chapterId}`
    );
    return new Chapter(data.docs[0], this.dataService);
  }

  public async findByBook(
    bookId: string,
    options?: FindConfig
  ): Promise<ChapterServiceResult> {
    const data = await this.dataService.fetchJSON(
      `https://the-one-api.dev/v2/book/${bookId}/chapter`,
      options
    );
    return {
      data: data.docs.map(
        (doc: ChapterDoc) => new Chapter(doc, this.dataService)
      ),
      metadata: {
        total: data.total,
        limit: data.limit,
        offset: data.offset,
        page: data.page,
        pages: data.pages,
      },
    };
  }
}
