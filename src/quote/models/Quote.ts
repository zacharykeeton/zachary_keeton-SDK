import QuoteDoc from "./QuoteDoc";
import MovieService from "../../movie/service";
import DataService from "../../data/service";
import Movie from "../../movie/models/Movie";
import CharacterService from "../../character/service";
import Character from "../../character/models/Character";

export default class Quote {
  _id: string;
  dialog: string;
  movieId: string;
  characterId: string;
  id: string;
  private fullMovie?: Movie;
  private fullCharacter?: Character;

  private movieService: MovieService;
  private characterService: CharacterService;

  constructor(doc: QuoteDoc, private requestService: DataService) {
    this._id = doc._id;
    this.dialog = doc.dialog;
    this.movieId = doc.movie;
    this.characterId = doc.character;
    this.id = doc.id;
    this.movieService = new MovieService(this.requestService);
    this.characterService = new CharacterService(this.requestService);
  }

  getMovie(): Promise<Movie> {
    if (this.fullMovie) {
      return Promise.resolve(this.fullMovie);
    } else {
      return this.movieService.findOne(this.movieId);
    }
  }

  getCharacter(): Promise<Character> {
    if (this.fullCharacter) {
      return Promise.resolve(this.fullCharacter);
    } else {
      return this.characterService.findOne(this.characterId);
    }
  }
}
