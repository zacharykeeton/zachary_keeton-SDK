export default interface QuoteDoc {
  _id: string;
  dialog: string;
  movie: string;
  character: string;
  id: string;
}
