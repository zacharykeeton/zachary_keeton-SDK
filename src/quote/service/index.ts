// QUOTE CLASS SHOULD BE ABLE TO GET MOVIES

// https://the-one-api.dev/documentation

import MovieService from "../../movie/service";
import DataService from "../../data/service";
import FindConfig from "../../data/models/FindConfig";
import ServiceResult from "../../models/ServiceResult";
import QuoteDoc from "../models/QuoteDoc";
import Quote from "../models/Quote";

export default class QuoteService {
  constructor(private requestService = new DataService()) {}

  public async find(options?: FindConfig): Promise<ServiceResult> {
    const data = await this.requestService.fetchJSON(
      "https://the-one-api.dev/v2/quote",
      options
    );

    return {
      data: data.docs.map(
        (doc: QuoteDoc) => new Quote(doc, this.requestService)
      ),
      metadata: {
        total: data.total,
        limit: data.limit,
        offset: data.offset,
        page: data.page,
        pages: data.pages,
      },
    };
  }

  public async findOne(quoteId: string) {
    const data = await this.requestService.fetchJSON(
      `https://the-one-api.dev/v2/quote/${quoteId}`
    );
    return new Quote(data.docs[0], this.requestService);
  }

  public async findByMovie(movieId: string, options?: FindConfig) {
    const data = await this.requestService.fetchJSON(
      `https://the-one-api.dev/v2/movie/${movieId}/quote`,
      options
    );
    return {
      data: data.docs.map(
        (doc: QuoteDoc) => new Quote(doc, this.requestService)
      ),
      metadata: {
        total: data.total,
        limit: data.limit,
        offset: data.offset,
        page: data.page,
        pages: data.pages,
      },
    };
  }

  public async findByCharacter(characterId: string) {
    const data = await this.requestService.fetchJSON(
      `https://the-one-api.dev/v2/character/${characterId}/quote`
    );
    return {
      data: data.docs.map(
        (doc: QuoteDoc) => new Quote(doc, this.requestService)
      ),
      metadata: {
        total: data.total,
        limit: data.limit,
        offset: data.offset,
        page: data.page,
        pages: data.pages,
      },
    };
  }
}
