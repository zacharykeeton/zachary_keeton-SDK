import * as dotenv from "dotenv"; // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
dotenv.config();

import { OneAPIClient } from ".";
import Book from "./book/models/Book";
import Chapter from "./chapter/models/Chapter";
import Character from "./character/models/Character";
import Movie from "./movie/models/Movie";
import Quote from "./quote/models/Quote";

describe("OneAPIClient", () => {
  jest.setTimeout(1000 * 1000);
  afterEach(() => {
    return new Promise((r) => {
      const ms = 6000;
      console.log(
        `WE'VE JUST HIT THE LIVE API. WAITING ${ms} BEFORE NEXT TEST TO RESPECT RATE LIMIT`
      );
      setTimeout(r, ms);
    });
  });

  const client = new OneAPIClient({ apiKey: process.env.API_KEY });
  describe(".books", () => {
    describe(".find", () => {
      it("fetches all books if no options sent", async () => {
        const result = await client.books.find();
        expect(result.metadata.total).toBe(3);
        expect(result.data.every((element) => element instanceof Book)).toBe(
          true
        );
      });
    });

    describe(".findOne", () => {
      it("fetches a book by id", async () => {
        const result = await client.books.findOne("5cf58080b53e011a64671584");
        expect(result instanceof Book).toBe(true);
        expect(result.name).toBe("The Return Of The King");
      });
    });
  });

  describe(".movies", () => {
    describe(".find", () => {
      it("fetches all movies if no options sent", async () => {
        const result = await client.movies.find();
        expect(result.metadata.total).toBe(8);
        expect(result.data.every((element) => element instanceof Movie)).toBe(
          true
        );
      });
    });

    describe(".findOne", () => {
      it("fetches a movie by id", async () => {
        const result = await client.movies.findOne("5cd95395de30eff6ebccde58");
        expect(result instanceof Movie).toBe(true);
        expect(result.name).toBe("The Unexpected Journey");
      });
    });
  });

  describe(".quotes", () => {
    describe(".find", () => {
      it("fetches all quotes if no options sent", async () => {
        const result = await client.quotes.find();
        expect(result.metadata.total).toBe(2390);
        expect(result.data.every((element) => element instanceof Quote)).toBe(
          true
        );
      });
    });

    describe(".findOne", () => {
      it("fetches a quote by id", async () => {
        const result = await client.quotes.findOne("5cd96e05de30eff6ebcce7e9");
        expect(result instanceof Quote).toBe(true);
        expect(result.dialog).toBe("Deagol!");
      });
    });

    describe(".findByMovie", () => {
      it("fetches quotes by a movie id", async () => {
        const result = await client.quotes.findByMovie(
          "5cd95395de30eff6ebccde5c" // Fellowship of the Ring
        );
        expect(result.metadata.total).toBe(507);
        expect(result.data.every((element) => element instanceof Quote)).toBe(
          true
        );
      });
    });

    describe(".findByCharacter", () => {
      it("fetches quotes by a character id", async () => {
        const result = await client.quotes.findByCharacter(
          "5cd99d4bde30eff6ebccfbe6" // Aragorn II Elessar
        );
        expect(result.metadata.total).toBe(214);
        expect(result.data.every((element) => element instanceof Quote)).toBe(
          true
        );
      });
    });
  });

  describe(".chapters", () => {
    describe(".find", () => {
      it("fetches all chapters if no options sent", async () => {
        const result = await client.chapters.find();
        expect(result.metadata.total).toBe(62);
        expect(result.data.every((element) => element instanceof Chapter)).toBe(
          true
        );
      });
    });

    describe(".findOne", () => {
      it("fetches a chapter by id", async () => {
        const result = await client.chapters.findOne(
          "6091b6d6d58360f988133b8d"
        );
        expect(result instanceof Chapter).toBe(true);
        expect(result.name).toBe("Three is Company");
      });
    });

    describe(".findByBook", () => {
      it("fetches chapters by book id", async () => {
        const result = await client.chapters.findByBook(
          "5cf58080b53e011a64671584" // Return of the King
        );
        expect(result.metadata.total).toBe(19);
        expect(result.data.every((element) => element instanceof Chapter)).toBe(
          true
        );
      });
    });
  });

  describe(".characters", () => {
    describe(".find", () => {
      it("fetches all characters if no options sent", async () => {
        const result = await client.characters.find();
        expect(result.metadata.total).toBe(933);
        expect(
          result.data.every((element) => element instanceof Character)
        ).toBe(true);
      });
    });

    describe(".findOne", () => {
      it("fetches a character by id", async () => {
        const result = await client.characters.findOne(
          "5cd99d4bde30eff6ebccfbe6" // Aragorn II Elessar
        );
        expect(result instanceof Character).toBe(true);
        expect(result.name).toBe("Aragorn II Elessar");
      });
    });
  });
});

// (async () => {
//   const client = new OneAPIClient({ apiKey: process.env.API_KEY });
//   // const requestService = new RequestService(process.env.API_KEY);

//   // const data = await requestService.fetchJSON(
//   //   "https://the-one-api.dev/v2/movie"
//   // );

//   //   const data = await requestService.getBooks();
//   // console.log(data);

//   // console.log("-----------BOOKS----------");
//   // const books = await client.books.getBooks();
//   // console.log(books);

//   //   console.log("-----------CHAPTERS----------");
//   //   const chapters = await books[0].getChapters();
//   //   console.log(chapters);

//   //   console.log("-----------GET BOOK----------");
//   //   const book = await client.getBook("5cf5805fb53e011a64671582");
//   //   console.log(book);

//   //   console.log("-----------GET CHAPTERS----------");
//   //   const chapters = await client.books.getChapters("5cf5805fb53e011a64671582");
//   //   console.log(chapters);

//   // console.log("-----------GET MOVIES----------");
//   // const movies = await client.movies.getMovies();
//   // console.log(movies);

//   // console.log("-----------GET MOVIE----------");
//   // const movie = await client.movies.getMovie("5cd95395de30eff6ebccde5d");
//   // console.log(movie);

//   // console.log("-----------GET QUOTES----------");
//   // const quotes = await client.movies.getQuotes("5cd95395de30eff6ebccde5d");
//   // console.log(quotes);

//   // console.log("-----------GET QUOTES----------");
//   // const quotes = await client.quotes.getQuotes();
//   // console.log(quotes);

//   // console.log("-----------GET QUOTE----------");
//   // const quote = await client.quotes.getQuote("5cd96e05de30eff6ebcce84b");
//   // console.log(quote);

//   // console.log("-----------GET QUOTE----------");
//   // const quote = await client.quotes.getQuote("5cd96e05de30eff6ebcce84b");
//   // console.log(quote);

//   // console.log("-----------GET CHAPTERS----------");
//   // const chapters = await client.chapters.getChapters();
//   // console.log(chapters);

//   // console.log("-----------GET CHARACTERS----------");
//   // const characters = await client.characters.getCharacters();
//   // console.log(characters);

//   // console.log("-----------GET CHARACTER----------");
//   // const character = await client.characters.getCharacter(
//   //   "5cd99d4bde30eff6ebccfc21"
//   // );
//   // console.log(character);

//   // console.log("-----------GET CHARACTER QUOTES----------");
//   // const quotes = await client.characters.getQuotes("5cd99d4bde30eff6ebccfc1b");
//   // console.log(quotes);
// })();
