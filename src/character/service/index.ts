// QUOTE CLASS SHOULD BE ABLE TO GET MOVIES

// https://the-one-api.dev/documentation

import MovieService from "../../movie/service";
import Quote from "../../quote/models/Quote";
import QuoteDoc from "../../quote/models/QuoteDoc";
import DataService from "../../data/service";
import CharacterDoc from "../models/CharacterDoc";
import Character from "../../character/models/Character";
import FindConfig from "../../data/models/FindConfig";
import ServiceResult from "../../models/ServiceResult";
export default class CharacterService {
  constructor(private requestService = new DataService()) {}

  public async find(options?: FindConfig): Promise<ServiceResult> {
    const data = await this.requestService.fetchJSON(
      "https://the-one-api.dev/v2/character",
      options
    );

    return {
      data: data.docs.map((doc: CharacterDoc) => new Character(doc)),
      metadata: {
        total: data.total,
        limit: data.limit,
        offset: data.offset,
        page: data.page,
        pages: data.pages,
      },
    };
  }

  public async findOne(characterId: string) {
    const data = await this.requestService.fetchJSON(
      `https://the-one-api.dev/v2/character/${characterId}`
    );
    return new Character(data.docs[0]);
  }
}
