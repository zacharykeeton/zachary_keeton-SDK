import CharacterDoc from "./CharacterDoc";

export default class Character {
  _id: string;
  height: string;
  race: string;
  gender: string;
  birth: string;
  spouse: string;
  death: string;
  realm: string;
  hair: string;
  name: string;
  wikiUrl: string;
  constructor(doc: CharacterDoc) {
    this._id = doc._id;
    this.height = doc.height;
    this.race = doc.race;
    this.gender = doc.gender;
    this.birth = doc.birth;
    this.spouse = doc.spouse;
    this.death = doc.death;
    this.realm = doc.realm;
    this.hair = doc.hair;
    this.name = doc.name;
    this.wikiUrl = doc.wikiUrl;
  }
}
