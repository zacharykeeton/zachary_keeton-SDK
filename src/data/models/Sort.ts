import SORT_DIRECTION from "../models/SORT_DIRECTION";

export default interface Sort {
  [key: string]: SORT_DIRECTION;
}
