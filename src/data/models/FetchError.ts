export default class FetchError extends Error {
  constructor(
    public url: string,
    public statusCode: number,
    public statusText: string
  ) {
    const message = `Error fetching ${url}. Return status: ${statusCode}: ${statusText}`;
    super(message);
  }
}
