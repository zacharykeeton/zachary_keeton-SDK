export default interface Pagination {
  limit?: number;
  page?: number;
  offset?: number;
}
