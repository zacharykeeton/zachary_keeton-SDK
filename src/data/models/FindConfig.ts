import Pagination from "../models/Pagination";
import Sort from "../models/Sort";
import Filter from "../models/Filter";

export default interface FindConfig {
  // bookId?: string;
  pagination?: Pagination;
  sort?: Sort;
  filter?: Filter;
}
