import FILTER_OPTIONS from "../models/FILTER_OPTIONS";

export default interface Filter {
  key: string;
  option: FILTER_OPTIONS;
  expression?: string | RegExp | string[] | number[];
}
