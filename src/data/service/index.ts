import fetch from "node-fetch";
import FetchError from "../models/FetchError";
import FindConfig from "../models/FindConfig";
import buildUrlWithQueryString from "./buildQueryString";

export default class DataService {
  constructor(private apiKey?: string) {}
  public async fetchJSON(url: string, options?: FindConfig) {
    const finalURL = buildUrlWithQueryString(url, options);
    const config = {
      headers: {
        Authorization: `Bearer ${this.apiKey}`,
      },
    };
    const response = await fetch(finalURL, config);
    if (!response.ok) {
      throw new FetchError(finalURL, response.status, response.statusText);
    }
    return response.json();
  }
}
