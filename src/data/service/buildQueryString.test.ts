import buildQueryString from "./buildQueryString";
import SORT_DIRECTION from "../models/SORT_DIRECTION";
import FILTER_OPTIONS from "../models/FILTER_OPTIONS";

const url = "https://the-one-api.dev/v2/book";

describe("buildQueryString", () => {
  it("exists", () => {
    expect(buildQueryString).toBeDefined();
  });

  it("returns url string if findConfig is falsy", () => {
    const result = buildQueryString(url);
    expect(result).toBe(url);
  });

  describe("sort", () => {
    it("renders multiple keys if set", () => {
      const expectedOutput = url + "?sort=name%3Aasc";

      const actualOutput = buildQueryString(url, {
        sort: {
          name: SORT_DIRECTION.ASC,
        },
      });

      expect(actualOutput).toBe(expectedOutput);
    });

    it("renders multiple keys if set", () => {
      const expectedOutput = url + "?sort=name%3Aasc%2Cage%3Adesc";

      const actualOutput = buildQueryString(url, {
        sort: {
          name: SORT_DIRECTION.ASC,
          age: SORT_DIRECTION.DESC,
        },
      });

      expect(actualOutput).toBe(expectedOutput);
    });

    it("does not renders if not set", () => {
      const expectedOutput = url + "";
      const actualOutput = buildQueryString(url, {});

      expect(actualOutput).toBe(expectedOutput);
    });

    it("does not render if undefined", () => {
      const expectedOutput = url + "";

      const actualOutput = buildQueryString(url, { sort: undefined });

      expect(actualOutput).toBe(expectedOutput);
    });

    it("does not renders if empty obj", () => {
      const expectedOutput = url + "";

      const actualOutput = buildQueryString(url, { sort: {} });

      expect(actualOutput).toBe(expectedOutput);
    });
  });

  describe("pagination", () => {
    describe("page", () => {
      it("renders if it is a number", () => {
        const expectedOutput = url + "?page=2";

        const actualOutput = buildQueryString(url, {
          pagination: {
            page: 2,
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });

      it("renders if it is zero", () => {
        const expectedOutput = url + "?page=0";

        const actualOutput = buildQueryString(url, {
          pagination: {
            page: 0,
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });
    });

    describe("limit", () => {
      it("renders if it is a number", () => {
        const expectedOutput = url + "?limit=2";

        const actualOutput = buildQueryString(url, {
          pagination: {
            limit: 2,
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });

      it("renders if it is zero", () => {
        const expectedOutput = url + "?limit=0";

        const actualOutput = buildQueryString(url, {
          pagination: {
            limit: 0,
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });
    });

    describe("offset", () => {
      it("renders if it is a number", () => {
        const expectedOutput = url + "?offset=2";

        const actualOutput = buildQueryString(url, {
          pagination: {
            offset: 2,
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });

      it("renders if it is zero", () => {
        const expectedOutput = url + "?offset=0";

        const actualOutput = buildQueryString(url, {
          pagination: {
            offset: 0,
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });
    });

    it("renders page, limit, and offset if set", () => {
      const expectedOutput = url + "?limit=1&page=2&offset=3";

      const actualOutput = buildQueryString(url, {
        pagination: {
          page: 2,
          limit: 1,
          offset: 3,
        },
      });

      expect(actualOutput).toBe(expectedOutput);
    });

    it("renders page, limit, and not offset if no offset", () => {
      const expectedOutput = url + "?limit=1&page=2";

      const actualOutput = buildQueryString(url, {
        pagination: {
          page: 2,
          limit: 1,
          // offset: 3,
        },
      });

      expect(actualOutput).toBe(expectedOutput);
    });

    it("renders page, offset, and not limit if no limit", () => {
      const expectedOutput = url + "?page=2&offset=3";

      const actualOutput = buildQueryString(url, {
        pagination: {
          page: 2,
          // limit: 1,
          offset: 3,
        },
      });

      expect(actualOutput).toBe(expectedOutput);
    });

    it("renders limit, offset, and not page if no page", () => {
      const expectedOutput = url + "?limit=1&offset=3";

      const actualOutput = buildQueryString(url, {
        pagination: {
          // page: 2,
          limit: 1,
          offset: 3,
        },
      });

      expect(actualOutput).toBe(expectedOutput);
    });
  });

  describe("filter", () => {
    describe("EXISTS", () => {
      it("renders correctly when the key is given", () => {
        const expectedOutput = url + "?name";

        const actualOutput = buildQueryString(url, {
          filter: {
            key: "name",
            option: FILTER_OPTIONS.EXISTS,
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });
    });

    describe("NOT_EXISTS", () => {
      it("renders correctly when the key is given", () => {
        const expectedOutput = url + "?!name";

        const actualOutput = buildQueryString(url, {
          filter: {
            key: "name",
            option: FILTER_OPTIONS.NOT_EXISTS,
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });
    });

    describe("MATCH_ANY", () => {
      it("renders correctly when the key and expression are strings", () => {
        const expectedOutput = url + "?name=Gandalf";

        const actualOutput = buildQueryString(url, {
          filter: {
            key: "name",
            option: FILTER_OPTIONS.MATCH_ANY,
            expression: "Gandalf",
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });

      it("renders empty when the expression is not passed", () => {
        const expectedOutput = url + "";

        const actualOutput = buildQueryString(url, {
          filter: {
            key: "name",
            option: FILTER_OPTIONS.MATCH_ANY,
            // expression: "someValue",
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });

      it("renders correctly when the expression is an array", () => {
        const expectedOutput = url + "?name=Frodo%2CSauron%2CGandalf";

        const actualOutput = buildQueryString(url, {
          filter: {
            key: "name",
            option: FILTER_OPTIONS.MATCH_ANY,
            expression: ["Frodo", "Sauron", "Gandalf"],
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });

      it("returns empty when the expression is an empty array", () => {
        const expectedOutput = url + "";

        const actualOutput = buildQueryString(url, {
          filter: {
            key: "name",
            option: FILTER_OPTIONS.MATCH_ANY,
            expression: [],
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });
    });

    describe("NOT_MATCH_ANY", () => {
      it("renders correctly when the key and expression are strings", () => {
        const expectedOutput = url + "?name!=Gandalf";

        const actualOutput = buildQueryString(url, {
          filter: {
            key: "name",
            option: FILTER_OPTIONS.NOT_MATCH_ANY,
            expression: "Gandalf",
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });

      it("renders empty when the expression is not passed", () => {
        const expectedOutput = url + "";

        const actualOutput = buildQueryString(url, {
          filter: {
            key: "name",
            option: FILTER_OPTIONS.NOT_MATCH_ANY,
            // expression: "someValue",
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });

      it("renders correctly when the expression is an array", () => {
        const expectedOutput = url + "?name!=Frodo%2CSauron%2CGandalf";

        const actualOutput = buildQueryString(url, {
          filter: {
            key: "name",
            option: FILTER_OPTIONS.NOT_MATCH_ANY,
            expression: ["Frodo", "Sauron", "Gandalf"],
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });

      it("returns empty when the expression is an empty array", () => {
        const expectedOutput = url + "";

        const actualOutput = buildQueryString(url, {
          filter: {
            key: "name",
            option: FILTER_OPTIONS.NOT_MATCH_ANY,
            expression: [],
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });
    });

    describe("LESS_THAN", () => {
      it("renders correctly when the key and expression are strings", () => {
        const expectedOutput = url + "?age%3C5";

        const actualOutput = buildQueryString(url, {
          filter: {
            key: "age",
            option: FILTER_OPTIONS.LESS_THAN,
            expression: "5",
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });
    });

    describe("LESS_OR_EQUAL", () => {
      it("renders correctly when the key and expression are strings", () => {
        const expectedOutput = url + "?age%3C=5";

        const actualOutput = buildQueryString(url, {
          filter: {
            key: "age",
            option: FILTER_OPTIONS.LESS_OR_EQUAL,
            expression: "5",
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });
    });

    describe("GREATER_THAN", () => {
      it("renders correctly when the key and expression are strings", () => {
        const expectedOutput = url + "?age%3E5";

        const actualOutput = buildQueryString(url, {
          filter: {
            key: "age",
            option: FILTER_OPTIONS.GREATER_THAN,
            expression: "5",
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });
    });

    describe("GREATER_OR_EQUAL", () => {
      it("renders correctly when the key and expression are strings", () => {
        const expectedOutput = url + "?age%3E=5";

        const actualOutput = buildQueryString(url, {
          filter: {
            key: "age",
            option: FILTER_OPTIONS.GREATER_OR_EQUAL,
            expression: "5",
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });
    });

    describe("REGEX", () => {
      it("renders correctly when the expression is a regex", () => {
        const expectedOutput = url + "?name=%2Ffoot%2Fi";

        const actualOutput = buildQueryString(url, {
          filter: {
            key: "name",
            option: FILTER_OPTIONS.REGEX,
            expression: /foot/i,
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });
    });

    describe("REGEX", () => {
      it("renders correctly when the expression is a string", () => {
        const expectedOutput = url + "?name=%2Ffoot%2Fi";

        const actualOutput = buildQueryString(url, {
          filter: {
            key: "name",
            option: FILTER_OPTIONS.REGEX,
            expression: /foot/i.toString(),
          },
        });

        expect(actualOutput).toBe(expectedOutput);
      });
    });
  });

  it("renders correctly with multiple things set", () => {
    const expectedOutput =
      url + "?limit=1&page=2&offset=3&sort=name%3Aasc&name!=zach";

    const actualOutput = buildQueryString(url, {
      pagination: {
        page: 2,
        limit: 1,
        offset: 3,
      },
      sort: { name: SORT_DIRECTION.ASC },
      filter: {
        key: "name",
        option: FILTER_OPTIONS.NOT_MATCH_ANY,
        expression: "zach",
      },
    });

    expect(actualOutput).toBe(expectedOutput);
  });

  it("renders correctly with nothing set", () => {
    const expectedOutput = url + "";

    const actualOutput = buildQueryString(url, {});

    expect(actualOutput).toBe(expectedOutput);
  });
});
