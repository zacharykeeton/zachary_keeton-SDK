/// <reference types="jest" />
import * as dotenv from "dotenv"; // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
import DataService from "./index";
import FetchError from "../models/FetchError";
import SORT_DIRECTION from "../models/SORT_DIRECTION";
import FILTER_OPTIONS from "../models/FILTER_OPTIONS";
dotenv.config();

/* 
       NOTE: THIS IS THE 'BOTTOM' OF THE STACK. SO, THESE TESTS HIT THE LIVE API. 
       KEEP THE 'afterEach' waits in there so we respect the API rate limit: (10req/min)
       // See https://the-one-api.dev/documentation
*/

describe("The DataService", () => {
  jest.setTimeout(100 * 1000);
  afterEach(() => {
    return new Promise((r) => {
      const ms = 5000;
      console.log(`JUST HIT THE LIVE API. WAITING ${ms} BEFORE NEXT TEST`);
      setTimeout(r, ms);
    });
  });

  it("exists", () => {
    expect(DataService).toBeDefined();
  });

  describe(".fetchJSON", () => {
    describe("vs Authenticated endpoints", () => {
      it("throw a FetchError with no apiKey", async () => {
        const dataService = new DataService();

        const url = "https://the-one-api.dev/v2/movie";
        const expectedStatusCode = 401;
        const expectedStatusText = "Unauthorized";

        const expectedFetchError = new FetchError(
          url,
          expectedStatusCode,
          expectedStatusText
        );

        await expect(dataService.fetchJSON(url)).rejects.toThrow(
          expectedFetchError
        );
      });

      it("throw a FetchError with invalid apiKey", async () => {
        const invalidKey = "gibberishKey";
        const dataService = new DataService(invalidKey);

        const url = "https://the-one-api.dev/v2/movie";
        const expectedStatusCode = 401;
        const expectedStatusText = "Unauthorized";

        const expectedFetchError = new FetchError(
          url,
          expectedStatusCode,
          expectedStatusText
        );

        await expect(dataService.fetchJSON(url)).rejects.toThrow(
          expectedFetchError
        );
      });

      it("resolve with valid apiKey", async () => {
        const validKey = process.env.API_KEY;
        const dataService = new DataService(validKey);
        const data = await dataService.fetchJSON(
          "https://the-one-api.dev/v2/movie"
        );
        // If no throw, all good
      });

      it("rejects with valid apiKey and bad endpoint", async () => {
        const validKey = process.env.API_KEY;
        const dataService = new DataService(validKey);

        const url = "https://the-one-api.dev/v2/badEndpoint3elss";
        const expectedStatusCode = 404;
        const expectedStatusText = "Not Found";

        const expectedFetchError = new FetchError(
          url,
          expectedStatusCode,
          expectedStatusText
        );

        await expect(dataService.fetchJSON(url)).rejects.toThrow(
          expectedFetchError
        );
      });
    });

    describe("vs Unauthenticated endpoints", () => {
      it("resolves with no apiKey", async () => {
        const dataService = new DataService();
        const data = await dataService.fetchJSON(
          "https://the-one-api.dev/v2/book"
        );
        console.log(data);
        // If no throw, all good
      });

      it("resolves with invalid apiKey", async () => {
        const dataService = new DataService("gibberishKey");
        const data = await dataService.fetchJSON(
          "https://the-one-api.dev/v2/book"
        );
        // If no throw, all good
      });

      describe("pagination", () => {
        it(".limit works", async () => {
          const dataService = new DataService();
          const data = await dataService.fetchJSON(
            "https://the-one-api.dev/v2/book",
            {
              pagination: {
                limit: 2,
              },
            }
          );

          // Avoid depending on actual DB records
          // expect(data.docs.length).toBe(2);
          expect(data.limit).toBe(2);
        });

        it(".limit with .page works", async () => {
          const dataService = new DataService();
          const data = await dataService.fetchJSON(
            "https://the-one-api.dev/v2/book",
            {
              pagination: {
                limit: 1,
                page: 2,
              },
            }
          );
          // Avoid depending on actual DB records
          // expect(data.docs.length).toBe(1);
          expect(data.limit).toBe(1);
          expect(data.page).toBe(2);
        });

        it(".limit with .offset works", async () => {
          const dataService = new DataService();
          const data = await dataService.fetchJSON(
            "https://the-one-api.dev/v2/book",
            {
              pagination: {
                limit: 1,
                offset: 2,
              },
            }
          );

          // Avoid depending on actual DB records
          // expect(data.docs.length).toBe(1);
          expect(data.limit).toBe(1);
          expect(data.offset).toBe(2);
        });
      });

      describe("sorting", () => {
        it("works asc", async () => {
          const dataService = new DataService();

          const unsortedData = await dataService.fetchJSON(
            "https://the-one-api.dev/v2/book"
          );
          expect(unsortedData.docs[0].name).toBe("The Fellowship Of The Ring");
          expect(unsortedData.docs[1].name).toBe("The Two Towers");
          expect(unsortedData.docs[2].name).toBe("The Return Of The King");

          const sortedData = await dataService.fetchJSON(
            "https://the-one-api.dev/v2/book",
            {
              sort: {
                name: SORT_DIRECTION.ASC,
              },
            }
          );

          // NOW THEY'RE IN ORDER
          expect(sortedData.docs[0].name).toBe("The Fellowship Of The Ring");
          expect(sortedData.docs[1].name).toBe("The Return Of The King");
          expect(sortedData.docs[2].name).toBe("The Two Towers");
        });

        it("works desc", async () => {
          const dataService = new DataService();

          const unsortedData = await dataService.fetchJSON(
            "https://the-one-api.dev/v2/book"
          );
          expect(unsortedData.docs[0].name).toBe("The Fellowship Of The Ring");
          expect(unsortedData.docs[1].name).toBe("The Two Towers");
          expect(unsortedData.docs[2].name).toBe("The Return Of The King");

          const sortedData = await dataService.fetchJSON(
            "https://the-one-api.dev/v2/book",
            {
              sort: {
                name: SORT_DIRECTION.DESC,
              },
            }
          );

          // NOW THEY'RE IN REVERSE ORDER
          expect(sortedData.docs[0].name).toBe("The Two Towers");
          expect(sortedData.docs[1].name).toBe("The Return Of The King");
          expect(sortedData.docs[2].name).toBe("The Fellowship Of The Ring");
        });
      });

      describe("filtering", () => {
        describe("MATCH_ANY", () => {
          it("returns found matches", async () => {
            const dataService = new DataService();
            const nameToFind = "The Two Towers";

            const data = await dataService.fetchJSON(
              "https://the-one-api.dev/v2/book",
              {
                filter: {
                  key: "name",
                  option: FILTER_OPTIONS.MATCH_ANY,
                  expression: nameToFind,
                },
              }
            );
            expect(data.total).toBe(1);
            expect(data.docs[0].name).toBe(nameToFind);
          });

          it("returns empty if no matches found", async () => {
            const dataService = new DataService();
            const nameToFind = "notARealName";

            const data = await dataService.fetchJSON(
              "https://the-one-api.dev/v2/book",
              {
                filter: {
                  key: "name",
                  option: FILTER_OPTIONS.MATCH_ANY,
                  expression: nameToFind,
                },
              }
            );
            expect(data.total).toBe(0);
            expect(data.docs.length).toBe(0);
          });

          it("does not filter if no expression passed", async () => {
            const dataService = new DataService();
            const nameToFind = "notARealName";

            const data = await dataService.fetchJSON(
              "https://the-one-api.dev/v2/book",
              {
                filter: {
                  key: "name",
                  option: FILTER_OPTIONS.MATCH_ANY,
                  // expression: nameToFind,
                },
              }
            );
            console.log(data);
            expect(data.total).toBe(3);
            expect(data.docs.length).toBe(3);
          });

          it("does not filter if expression is empty string", async () => {
            const dataService = new DataService();
            const nameToFind = "";

            const data = await dataService.fetchJSON(
              "https://the-one-api.dev/v2/book",
              {
                filter: {
                  key: "name",
                  option: FILTER_OPTIONS.MATCH_ANY,
                  expression: nameToFind,
                },
              }
            );
            expect(data.total).toBe(3);
            expect(data.docs.length).toBe(3);
          });
        });

        describe.only("NOT_MATCH_ANY", () => {
          it("returns found non-matches", async () => {
            const dataService = new DataService();
            const nameToNotFind = "The Two Towers";

            const data = await dataService.fetchJSON(
              "https://the-one-api.dev/v2/book",
              {
                filter: {
                  key: "name",
                  option: FILTER_OPTIONS.NOT_MATCH_ANY,
                  expression: nameToNotFind,
                },
              }
            );
            expect(data.total).toBe(2);
            expect(
              data.docs.some((doc: any) => doc.name === nameToNotFind)
            ).toBe(false);
          });

          it("does not filter if no expression passed", async () => {
            const dataService = new DataService();
            const nameToNotFind = "The Two Towers";

            const data = await dataService.fetchJSON(
              "https://the-one-api.dev/v2/book",
              {
                filter: {
                  key: "name",
                  option: FILTER_OPTIONS.NOT_MATCH_ANY,
                  // expression: nameToNotFind,
                },
              }
            );
            expect(data.total).toBe(3);
            expect(data.docs.length).toBe(3);
          });

          it("does not filter if expression is empty string", async () => {
            const dataService = new DataService();
            const nameToNotFind = "";

            const data = await dataService.fetchJSON(
              "https://the-one-api.dev/v2/book",
              {
                filter: {
                  key: "name",
                  option: FILTER_OPTIONS.NOT_MATCH_ANY,
                  // expression: nameToNotFind,
                },
              }
            );
            expect(data.total).toBe(3);
            expect(data.docs.length).toBe(3);
          });
        });
      });

      it("rejects if endpoint not found", async () => {
        const dataService = new DataService("gibberishKey");
        const url = "https://the-one-api.dev/v2/nonexistentendpoint";
        const expectedStatusCode = 404;
        const expectedStatusText = "Not Found";
        await expect(dataService.fetchJSON(url)).rejects.toThrow(
          `Error fetching ${url}. Return status: ${expectedStatusCode}: ${expectedStatusText}`
        );
      });
    });
  });
});
