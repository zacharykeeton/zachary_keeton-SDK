import * as querystring from "node:querystring";
import FILTER_OPTIONS from "../models/FILTER_OPTIONS";
import appendQuery from "append-query";
import Filter from "../models/Filter";
import FindConfig from "../models/FindConfig";

const objIsEmpty = (obj: any) => {
  return (
    obj &&
    Object.keys(obj).length === 0 &&
    Object.getPrototypeOf(obj) === Object.prototype
  );
};

const getFilterString = (filterObj?: Filter) => {
  if (!filterObj) return "";
  switch (filterObj.option) {
    // name
    case FILTER_OPTIONS.EXISTS:
      return querystring.escape(filterObj.key);
      break;

    // !name
    case FILTER_OPTIONS.NOT_EXISTS:
      return `!${querystring.escape(filterObj.key)}`;
      break;

    // name=Gandalf
    // race=Hobbit,Human
    case FILTER_OPTIONS.MATCH_ANY:
      if (typeof filterObj.expression === "string") {
        return querystring.encode({
          [filterObj.key]: filterObj.expression,
        });
      }
      if (Array.isArray(filterObj.expression) && filterObj.expression.length) {
        return `${filterObj.key}=${filterObj.expression.join(",")}`;
      }
      break;

    // name!=Frodo
    // race!=Orc,Goblin
    case FILTER_OPTIONS.NOT_MATCH_ANY:
      if (typeof filterObj.expression === "string") {
        return `${filterObj.key}!=${filterObj.expression}`;
      }
      if (Array.isArray(filterObj.expression) && filterObj.expression.length) {
        return `${filterObj.key}!=${filterObj.expression.join(",")}`;
      }
      break;

    case FILTER_OPTIONS.LESS_THAN:
      return querystring.stringify(
        {
          [filterObj.key]: filterObj?.expression?.toString(),
        },
        undefined,
        "<"
      );
      break;

    case FILTER_OPTIONS.LESS_OR_EQUAL:
      return `${filterObj.key}<=${filterObj.expression}`;
      break;

    case FILTER_OPTIONS.GREATER_THAN:
      return `${filterObj.key}>${filterObj.expression}`;
      break;

    case FILTER_OPTIONS.GREATER_OR_EQUAL:
      return `${filterObj.key}>=${filterObj.expression}`;
      break;

    case FILTER_OPTIONS.REGEX:
      if (
        filterObj.expression instanceof RegExp ||
        typeof filterObj.expression === "string"
      ) {
        return `${filterObj.key}=${filterObj.expression.toString()}`;
      }
      break;

    default:
      break;
  }
};

export default function buildUrlWithQueryString(
  url: string,
  config?: FindConfig
): string {
  if (!config) return url;

  const flattendObj: {
    limit?: number;
    page?: number;
    offset?: number;
    sort?: string;
    filter?: string;
  } = {};

  // LIMIT
  const limit = config?.pagination?.limit;
  typeof limit === "number" && (flattendObj.limit = limit);

  // PAGE
  const page = config?.pagination?.page;
  typeof page === "number" && (flattendObj.page = page);

  // OFFSET
  const offset = config?.pagination?.offset;
  typeof offset === "number" && (flattendObj.offset = offset);

  // SORT
  const sort = config?.sort;
  if (sort && !objIsEmpty(sort)) {
    // const firstKey = Object.keys(sort)[0]
    // const value = sort[firstKey]
    flattendObj.sort = querystring.encode(config?.sort, ",", ":");
  }

  let result = appendQuery(url, querystring.stringify(flattendObj));

  // FILTER
  const filterString = getFilterString(config?.filter);

  if (filterString) {
    if (
      [
        FILTER_OPTIONS.EXISTS,
        FILTER_OPTIONS.NOT_EXISTS,
        FILTER_OPTIONS.LESS_THAN,
        FILTER_OPTIONS.GREATER_THAN,
      ].includes(config?.filter?.option!)
    ) {
      result = appendQuery(result, { [filterString]: null });
    } else {
      result = appendQuery(result, filterString);
    }
  }

  return result;
}
