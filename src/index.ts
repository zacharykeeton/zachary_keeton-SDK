// https://the-one-api.dev/documentation

import DataService from "./data/service";
import BookService from "./book/service";
import QuoteService from "./quote/service";
import ChapterService from "./chapter/service";
import CharacterService from "./character/service";
import MovieService from "./movie/service";
import OneAPIClientConfig from "./models/OneAPIClientConfig";

export { default as SORT_DIRECTION } from "./data/models/SORT_DIRECTION";
export { default as FILTER_OPTIONS } from "./data/models/FILTER_OPTIONS";

/**
 * The OneAPIClient class
 */
export class OneAPIClient {
  private readonly dataService: DataService;

  public books: BookService;
  public movies: MovieService;
  public quotes: QuoteService;
  public chapters: ChapterService;
  public characters: CharacterService;

  constructor({ requestService, apiKey }: OneAPIClientConfig = {}) {
    this.dataService = requestService ?? new DataService(apiKey);
    this.books = new BookService(this.dataService);
    this.movies = new MovieService(this.dataService);
    this.quotes = new QuoteService(this.dataService);
    this.chapters = new ChapterService(this.dataService);
    this.characters = new CharacterService(this.dataService);
  }
}
