## zachary_keeton-sdk

- [Home](home)
- [Exports](Exports)

### Modules

- [<internal>](modules/%3Cinternal%3E)

### Enumerations

- [FILTER_OPTIONS](enums/FILTER_OPTIONS)
- [SORT_DIRECTION](enums/SORT_DIRECTION)

### Classes

- [Book](classes/%3Cinternal%3E.Book)
- [BookService](classes/%3Cinternal%3E.BookService)
- [Chapter](classes/%3Cinternal%3E.Chapter)
- [ChapterService](classes/%3Cinternal%3E.ChapterService)
- [Character](classes/%3Cinternal%3E.Character)
- [CharacterService](classes/%3Cinternal%3E.CharacterService)
- [DataService](classes/%3Cinternal%3E.DataService)
- [Movie](classes/%3Cinternal%3E.Movie)
- [MovieService](classes/%3Cinternal%3E.MovieService)
- [Quote](classes/%3Cinternal%3E.Quote)
- [QuoteService](classes/%3Cinternal%3E.QuoteService)
- [OneAPIClient](classes/OneAPIClient)

### Interfaces

- [ChapterDoc](interfaces/%3Cinternal%3E.ChapterDoc)
- [ChapterServiceResult](interfaces/%3Cinternal%3E.ChapterServiceResult)
- [CharacterDoc](interfaces/%3Cinternal%3E.CharacterDoc)
- [Doc](interfaces/%3Cinternal%3E.Doc)
- [Filter](interfaces/%3Cinternal%3E.Filter)
- [FindConfig](interfaces/%3Cinternal%3E.FindConfig)
- [MovieDoc](interfaces/%3Cinternal%3E.MovieDoc)
- [OneAPIClientConfig](interfaces/%3Cinternal%3E.OneAPIClientConfig)
- [Pagination](interfaces/%3Cinternal%3E.Pagination)
- [QuoteDoc](interfaces/%3Cinternal%3E.QuoteDoc)
- [Service](interfaces/%3Cinternal%3E.Service)
- [ServiceResult](interfaces/%3Cinternal%3E.ServiceResult)
- [Sort](interfaces/%3Cinternal%3E.Sort)