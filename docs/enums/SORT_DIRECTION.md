## Table of contents

### Enumeration Members

- [ASC](./SORT_DIRECTION#asc)
- [DESC](./SORT_DIRECTION#desc)

## Enumeration Members

### ASC

• **ASC** = ``"asc"``

#### Defined in

[data/models/SORT_DIRECTION.ts:2](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/SORT_DIRECTION.ts#L2)

___

### DESC

• **DESC** = ``"desc"``

#### Defined in

[data/models/SORT_DIRECTION.ts:3](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/SORT_DIRECTION.ts#L3)
