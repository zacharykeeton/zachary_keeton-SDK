## Table of contents

### Enumeration Members

- [EXISTS](./FILTER_OPTIONS#exists)
- [GREATER\_OR\_EQUAL](./FILTER_OPTIONS#greater_or_equal)
- [GREATER\_THAN](./FILTER_OPTIONS#greater_than)
- [LESS\_OR\_EQUAL](./FILTER_OPTIONS#less_or_equal)
- [LESS\_THAN](./FILTER_OPTIONS#less_than)
- [MATCH\_ANY](./FILTER_OPTIONS#match_any)
- [NOT\_EXISTS](./FILTER_OPTIONS#not_exists)
- [NOT\_MATCH\_ANY](./FILTER_OPTIONS#not_match_any)
- [REGEX](./FILTER_OPTIONS#regex)

## Enumeration Members

### EXISTS

• **EXISTS** = ``"EXISTS"``

#### Defined in

[data/models/FILTER_OPTIONS.ts:4](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/FILTER_OPTIONS.ts#L4)

___

### GREATER\_OR\_EQUAL

• **GREATER\_OR\_EQUAL** = ``"GREATER_OR_EQUAL"``

#### Defined in

[data/models/FILTER_OPTIONS.ts:10](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/FILTER_OPTIONS.ts#L10)

___

### GREATER\_THAN

• **GREATER\_THAN** = ``"GREATER_THAN"``

#### Defined in

[data/models/FILTER_OPTIONS.ts:9](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/FILTER_OPTIONS.ts#L9)

___

### LESS\_OR\_EQUAL

• **LESS\_OR\_EQUAL** = ``"LESS_OR_EQUAL"``

#### Defined in

[data/models/FILTER_OPTIONS.ts:8](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/FILTER_OPTIONS.ts#L8)

___

### LESS\_THAN

• **LESS\_THAN** = ``"LESS_THAN"``

#### Defined in

[data/models/FILTER_OPTIONS.ts:7](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/FILTER_OPTIONS.ts#L7)

___

### MATCH\_ANY

• **MATCH\_ANY** = ``"MATCH_ANY"``

#### Defined in

[data/models/FILTER_OPTIONS.ts:2](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/FILTER_OPTIONS.ts#L2)

___

### NOT\_EXISTS

• **NOT\_EXISTS** = ``"NOT_EXISTS"``

#### Defined in

[data/models/FILTER_OPTIONS.ts:5](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/FILTER_OPTIONS.ts#L5)

___

### NOT\_MATCH\_ANY

• **NOT\_MATCH\_ANY** = ``"NOT_MATCH_ANY"``

#### Defined in

[data/models/FILTER_OPTIONS.ts:3](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/FILTER_OPTIONS.ts#L3)

___

### REGEX

• **REGEX** = ``"REGEX"``

#### Defined in

[data/models/FILTER_OPTIONS.ts:6](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/FILTER_OPTIONS.ts#L6)
