## Table of contents

### Classes

- [Book](../classes/&lt;internal&gt;.Book)
- [BookService](../classes/&lt;internal&gt;.BookService)
- [Chapter](../classes/&lt;internal&gt;.Chapter)
- [ChapterService](../classes/&lt;internal&gt;.ChapterService)
- [Character](../classes/&lt;internal&gt;.Character)
- [CharacterService](../classes/&lt;internal&gt;.CharacterService)
- [DataService](../classes/&lt;internal&gt;.DataService)
- [Movie](../classes/&lt;internal&gt;.Movie)
- [MovieService](../classes/&lt;internal&gt;.MovieService)
- [Quote](../classes/&lt;internal&gt;.Quote)
- [QuoteService](../classes/&lt;internal&gt;.QuoteService)

### Interfaces

- [ChapterDoc](../interfaces/&lt;internal&gt;.ChapterDoc)
- [ChapterServiceResult](../interfaces/&lt;internal&gt;.ChapterServiceResult)
- [CharacterDoc](../interfaces/&lt;internal&gt;.CharacterDoc)
- [Doc](../interfaces/&lt;internal&gt;.Doc)
- [Filter](../interfaces/&lt;internal&gt;.Filter)
- [FindConfig](../interfaces/&lt;internal&gt;.FindConfig)
- [MovieDoc](../interfaces/&lt;internal&gt;.MovieDoc)
- [OneAPIClientConfig](../interfaces/&lt;internal&gt;.OneAPIClientConfig)
- [Pagination](../interfaces/&lt;internal&gt;.Pagination)
- [QuoteDoc](../interfaces/&lt;internal&gt;.QuoteDoc)
- [Service](../interfaces/&lt;internal&gt;.Service)
- [ServiceResult](../interfaces/&lt;internal&gt;.ServiceResult)
- [Sort](../interfaces/&lt;internal&gt;.Sort)
