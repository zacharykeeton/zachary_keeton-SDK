[<internal>](../modules/<internal>).QuoteService

## Table of contents

### Constructors

- [constructor](./&lt;internal&gt;.QuoteService#constructor)

### Methods

- [find](./&lt;internal&gt;.QuoteService#find)
- [findByCharacter](./&lt;internal&gt;.QuoteService#findbycharacter)
- [findByMovie](./&lt;internal&gt;.QuoteService#findbymovie)
- [findOne](./&lt;internal&gt;.QuoteService#findone)

## Constructors

### constructor

• **new QuoteService**(`requestService?`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `requestService` | [`DataService`](./<internal>.DataService) |

#### Defined in

[quote/service/index.ts:13](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/service/index.ts#L13)

## Methods

### find

▸ **find**(`options?`): `Promise`<[`ServiceResult`](../interfaces/<internal>.ServiceResult)\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `options?` | [`FindConfig`](../interfaces/<internal>.FindConfig) |

#### Returns

`Promise`<[`ServiceResult`](../interfaces/<internal>.ServiceResult)\>

#### Defined in

[quote/service/index.ts:15](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/service/index.ts#L15)

___

### findByCharacter

▸ **findByCharacter**(`characterId`): `Promise`<{ `data`: `any` ; `metadata`: { `limit`: `any` = data.limit; `offset`: `any` = data.offset; `page`: `any` = data.page; `pages`: `any` = data.pages; `total`: `any` = data.total }  }\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `characterId` | `string` |

#### Returns

`Promise`<{ `data`: `any` ; `metadata`: { `limit`: `any` = data.limit; `offset`: `any` = data.offset; `page`: `any` = data.page; `pages`: `any` = data.pages; `total`: `any` = data.total }  }\>

#### Defined in

[quote/service/index.ts:61](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/service/index.ts#L61)

___

### findByMovie

▸ **findByMovie**(`movieId`, `options?`): `Promise`<{ `data`: `any` ; `metadata`: { `limit`: `any` = data.limit; `offset`: `any` = data.offset; `page`: `any` = data.page; `pages`: `any` = data.pages; `total`: `any` = data.total }  }\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `movieId` | `string` |
| `options?` | [`FindConfig`](../interfaces/<internal>.FindConfig) |

#### Returns

`Promise`<{ `data`: `any` ; `metadata`: { `limit`: `any` = data.limit; `offset`: `any` = data.offset; `page`: `any` = data.page; `pages`: `any` = data.pages; `total`: `any` = data.total }  }\>

#### Defined in

[quote/service/index.ts:42](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/service/index.ts#L42)

___

### findOne

▸ **findOne**(`quoteId`): `Promise`<[`Quote`](./<internal>.Quote)\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `quoteId` | `string` |

#### Returns

`Promise`<[`Quote`](./<internal>.Quote)\>

#### Defined in

[quote/service/index.ts:35](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/service/index.ts#L35)
