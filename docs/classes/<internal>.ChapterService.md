[<internal>](../modules/<internal>).ChapterService

Chapter Service

## Table of contents

### Constructors

- [constructor](./&lt;internal&gt;.ChapterService#constructor)

### Methods

- [find](./&lt;internal&gt;.ChapterService#find)
- [findByBook](./&lt;internal&gt;.ChapterService#findbybook)
- [findOne](./&lt;internal&gt;.ChapterService#findone)

## Constructors

### constructor

• **new ChapterService**(`dataService?`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `dataService` | [`DataService`](./<internal>.DataService) |

#### Defined in

[chapter/service/index.ts:17](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/chapter/service/index.ts#L17)

## Methods

### find

▸ **find**(`options?`): `Promise`<[`ChapterServiceResult`](../interfaces/<internal>.ChapterServiceResult)\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `options?` | [`FindConfig`](../interfaces/<internal>.FindConfig) |

#### Returns

`Promise`<[`ChapterServiceResult`](../interfaces/<internal>.ChapterServiceResult)\>

#### Defined in

[chapter/service/index.ts:19](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/chapter/service/index.ts#L19)

___

### findByBook

▸ **findByBook**(`bookId`, `options?`): `Promise`<[`ChapterServiceResult`](../interfaces/<internal>.ChapterServiceResult)\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `bookId` | `string` |
| `options?` | [`FindConfig`](../interfaces/<internal>.FindConfig) |

#### Returns

`Promise`<[`ChapterServiceResult`](../interfaces/<internal>.ChapterServiceResult)\>

#### Defined in

[chapter/service/index.ts:45](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/chapter/service/index.ts#L45)

___

### findOne

▸ **findOne**(`chapterId`): `Promise`<[`Chapter`](./<internal>.Chapter)\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `chapterId` | `string` |

#### Returns

`Promise`<[`Chapter`](./<internal>.Chapter)\>

#### Defined in

[chapter/service/index.ts:38](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/chapter/service/index.ts#L38)
