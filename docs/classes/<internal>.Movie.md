[<internal>](../modules/<internal>).Movie

## Table of contents

### Constructors

- [constructor](./&lt;internal&gt;.Movie#constructor)

### Properties

- [\_id](./&lt;internal&gt;.Movie#_id)
- [academyAwardNominations](./&lt;internal&gt;.Movie#academyawardnominations)
- [academyAwardWins](./&lt;internal&gt;.Movie#academyawardwins)
- [boxOfficeRevenueInMillions](./&lt;internal&gt;.Movie#boxofficerevenueinmillions)
- [budgetInMillions](./&lt;internal&gt;.Movie#budgetinmillions)
- [name](./&lt;internal&gt;.Movie#name)
- [rottenTomatoesScore](./&lt;internal&gt;.Movie#rottentomatoesscore)
- [runtimeInMinutes](./&lt;internal&gt;.Movie#runtimeinminutes)

## Constructors

### constructor

• **new Movie**(`doc`, `requestService`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `doc` | [`MovieDoc`](../interfaces/<internal>.MovieDoc) |
| `requestService` | [`DataService`](./<internal>.DataService) |

#### Defined in

[movie/models/Movie.ts:14](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/models/Movie.ts#L14)

## Properties

### \_id

• **\_id**: `string`

#### Defined in

[movie/models/Movie.ts:5](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/models/Movie.ts#L5)

___

### academyAwardNominations

• **academyAwardNominations**: `number`

#### Defined in

[movie/models/Movie.ts:10](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/models/Movie.ts#L10)

___

### academyAwardWins

• **academyAwardWins**: `number`

#### Defined in

[movie/models/Movie.ts:11](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/models/Movie.ts#L11)

___

### boxOfficeRevenueInMillions

• **boxOfficeRevenueInMillions**: `number`

#### Defined in

[movie/models/Movie.ts:9](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/models/Movie.ts#L9)

___

### budgetInMillions

• **budgetInMillions**: `number`

#### Defined in

[movie/models/Movie.ts:8](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/models/Movie.ts#L8)

___

### name

• **name**: `string`

#### Defined in

[movie/models/Movie.ts:6](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/models/Movie.ts#L6)

___

### rottenTomatoesScore

• **rottenTomatoesScore**: `number`

#### Defined in

[movie/models/Movie.ts:12](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/models/Movie.ts#L12)

___

### runtimeInMinutes

• **runtimeInMinutes**: `number`

#### Defined in

[movie/models/Movie.ts:7](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/models/Movie.ts#L7)
