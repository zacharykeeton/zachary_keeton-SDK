[<internal>](../modules/<internal>).BookService

The BookService Class

## Implements

- [`Service`](../interfaces/<internal>.Service)

## Table of contents

### Constructors

- [constructor](./&lt;internal&gt;.BookService#constructor)

### Methods

- [find](./&lt;internal&gt;.BookService#find)
- [findOne](./&lt;internal&gt;.BookService#findone)

## Constructors

### constructor

• **new BookService**(`requestService?`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `requestService` | [`DataService`](./<internal>.DataService) |

#### Defined in

[book/service/index.ts:18](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/book/service/index.ts#L18)

## Methods

### find

▸ **find**(`options?`): `Promise`<[`ServiceResult`](../interfaces/<internal>.ServiceResult)\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `options?` | [`FindConfig`](../interfaces/<internal>.FindConfig) |

#### Returns

`Promise`<[`ServiceResult`](../interfaces/<internal>.ServiceResult)\>

#### Implementation of

Service.find

#### Defined in

[book/service/index.ts:20](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/book/service/index.ts#L20)

___

### findOne

▸ **findOne**(`bookId`): `Promise`<[`Book`](./<internal>.Book)\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `bookId` | `string` |

#### Returns

`Promise`<[`Book`](./<internal>.Book)\>

#### Defined in

[book/service/index.ts:37](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/book/service/index.ts#L37)
