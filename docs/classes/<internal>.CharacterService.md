[<internal>](../modules/<internal>).CharacterService

## Table of contents

### Constructors

- [constructor](./&lt;internal&gt;.CharacterService#constructor)

### Methods

- [find](./&lt;internal&gt;.CharacterService#find)
- [findOne](./&lt;internal&gt;.CharacterService#findone)

## Constructors

### constructor

• **new CharacterService**(`requestService?`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `requestService` | [`DataService`](./<internal>.DataService) |

#### Defined in

[character/service/index.ts:14](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/service/index.ts#L14)

## Methods

### find

▸ **find**(`options?`): `Promise`<[`ServiceResult`](../interfaces/<internal>.ServiceResult)\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `options?` | [`FindConfig`](../interfaces/<internal>.FindConfig) |

#### Returns

`Promise`<[`ServiceResult`](../interfaces/<internal>.ServiceResult)\>

#### Defined in

[character/service/index.ts:16](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/service/index.ts#L16)

___

### findOne

▸ **findOne**(`characterId`): `Promise`<[`Character`](./<internal>.Character)\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `characterId` | `string` |

#### Returns

`Promise`<[`Character`](./<internal>.Character)\>

#### Defined in

[character/service/index.ts:34](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/service/index.ts#L34)
