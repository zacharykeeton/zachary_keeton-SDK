[<internal>](../modules/<internal>).Character

## Table of contents

### Constructors

- [constructor](./&lt;internal&gt;.Character#constructor)

### Properties

- [\_id](./&lt;internal&gt;.Character#_id)
- [birth](./&lt;internal&gt;.Character#birth)
- [death](./&lt;internal&gt;.Character#death)
- [gender](./&lt;internal&gt;.Character#gender)
- [hair](./&lt;internal&gt;.Character#hair)
- [height](./&lt;internal&gt;.Character#height)
- [name](./&lt;internal&gt;.Character#name)
- [race](./&lt;internal&gt;.Character#race)
- [realm](./&lt;internal&gt;.Character#realm)
- [spouse](./&lt;internal&gt;.Character#spouse)
- [wikiUrl](./&lt;internal&gt;.Character#wikiurl)

## Constructors

### constructor

• **new Character**(`doc`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `doc` | [`CharacterDoc`](../interfaces/<internal>.CharacterDoc) |

#### Defined in

[character/models/Character.ts:15](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/Character.ts#L15)

## Properties

### \_id

• **\_id**: `string`

#### Defined in

[character/models/Character.ts:4](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/Character.ts#L4)

___

### birth

• **birth**: `string`

#### Defined in

[character/models/Character.ts:8](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/Character.ts#L8)

___

### death

• **death**: `string`

#### Defined in

[character/models/Character.ts:10](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/Character.ts#L10)

___

### gender

• **gender**: `string`

#### Defined in

[character/models/Character.ts:7](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/Character.ts#L7)

___

### hair

• **hair**: `string`

#### Defined in

[character/models/Character.ts:12](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/Character.ts#L12)

___

### height

• **height**: `string`

#### Defined in

[character/models/Character.ts:5](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/Character.ts#L5)

___

### name

• **name**: `string`

#### Defined in

[character/models/Character.ts:13](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/Character.ts#L13)

___

### race

• **race**: `string`

#### Defined in

[character/models/Character.ts:6](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/Character.ts#L6)

___

### realm

• **realm**: `string`

#### Defined in

[character/models/Character.ts:11](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/Character.ts#L11)

___

### spouse

• **spouse**: `string`

#### Defined in

[character/models/Character.ts:9](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/Character.ts#L9)

___

### wikiUrl

• **wikiUrl**: `string`

#### Defined in

[character/models/Character.ts:14](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/Character.ts#L14)
