[<internal>](../modules/<internal>).Book

## Table of contents

### Constructors

- [constructor](./&lt;internal&gt;.Book#constructor)

### Properties

- [\_id](./&lt;internal&gt;.Book#_id)
- [name](./&lt;internal&gt;.Book#name)

### Methods

- [getChapters](./&lt;internal&gt;.Book#getchapters)

## Constructors

### constructor

• **new Book**(`_id`, `name`, `dataService`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `_id` | `string` |
| `name` | `string` |
| `dataService` | [`DataService`](./<internal>.DataService) |

#### Defined in

[book/models/Book.ts:8](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/book/models/Book.ts#L8)

## Properties

### \_id

• `Readonly` **\_id**: `string`

#### Defined in

[book/models/Book.ts:9](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/book/models/Book.ts#L9)

___

### name

• `Readonly` **name**: `string`

#### Defined in

[book/models/Book.ts:10](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/book/models/Book.ts#L10)

## Methods

### getChapters

▸ **getChapters**(): `Promise`<[`Chapter`](./<internal>.Chapter)[]\>

#### Returns

`Promise`<[`Chapter`](./<internal>.Chapter)[]\>

#### Defined in

[book/models/Book.ts:15](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/book/models/Book.ts#L15)
