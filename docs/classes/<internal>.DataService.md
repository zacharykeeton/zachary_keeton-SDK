[<internal>](../modules/<internal>).DataService

## Table of contents

### Constructors

- [constructor](./&lt;internal&gt;.DataService#constructor)

### Methods

- [fetchJSON](./&lt;internal&gt;.DataService#fetchjson)

## Constructors

### constructor

• **new DataService**(`apiKey?`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `apiKey?` | `string` |

#### Defined in

[data/service/index.ts:7](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/service/index.ts#L7)

## Methods

### fetchJSON

▸ **fetchJSON**(`url`, `options?`): `Promise`<`any`\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `url` | `string` |
| `options?` | [`FindConfig`](../interfaces/<internal>.FindConfig) |

#### Returns

`Promise`<`any`\>

#### Defined in

[data/service/index.ts:8](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/service/index.ts#L8)
