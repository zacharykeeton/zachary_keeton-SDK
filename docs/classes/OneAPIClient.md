The OneAPIClient class

## Table of contents

### Constructors

- [constructor](./OneAPIClient#constructor)

### Properties

- [books](./OneAPIClient#books)
- [chapters](./OneAPIClient#chapters)
- [characters](./OneAPIClient#characters)
- [movies](./OneAPIClient#movies)
- [quotes](./OneAPIClient#quotes)

## Constructors

### constructor

• **new OneAPIClient**(`__namedParameters?`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`OneAPIClientConfig`](../interfaces/<internal>.OneAPIClientConfig) |

#### Defined in

[index.ts:26](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/index.ts#L26)

## Properties

### books

• **books**: [`BookService`](./<internal>.BookService)

#### Defined in

[index.ts:20](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/index.ts#L20)

___

### chapters

• **chapters**: [`ChapterService`](./<internal>.ChapterService)

#### Defined in

[index.ts:23](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/index.ts#L23)

___

### characters

• **characters**: [`CharacterService`](./<internal>.CharacterService)

#### Defined in

[index.ts:24](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/index.ts#L24)

___

### movies

• **movies**: [`MovieService`](./<internal>.MovieService)

#### Defined in

[index.ts:21](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/index.ts#L21)

___

### quotes

• **quotes**: [`QuoteService`](./<internal>.QuoteService)

#### Defined in

[index.ts:22](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/index.ts#L22)
