[<internal>](../modules/<internal>).MovieService

## Table of contents

### Constructors

- [constructor](./&lt;internal&gt;.MovieService#constructor)

### Properties

- [requestService](./&lt;internal&gt;.MovieService#requestservice)

### Methods

- [find](./&lt;internal&gt;.MovieService#find)
- [findOne](./&lt;internal&gt;.MovieService#findone)

## Constructors

### constructor

• **new MovieService**(`requestService?`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `requestService` | [`DataService`](./<internal>.DataService) |

#### Defined in

[movie/service/index.ts:14](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/service/index.ts#L14)

## Properties

### requestService

• **requestService**: [`DataService`](./<internal>.DataService)

#### Defined in

[movie/service/index.ts:14](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/service/index.ts#L14)

## Methods

### find

▸ **find**(`options?`): `Promise`<[`ServiceResult`](../interfaces/<internal>.ServiceResult)\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `options?` | [`FindConfig`](../interfaces/<internal>.FindConfig) |

#### Returns

`Promise`<[`ServiceResult`](../interfaces/<internal>.ServiceResult)\>

#### Defined in

[movie/service/index.ts:16](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/service/index.ts#L16)

___

### findOne

▸ **findOne**(`movieId`): `Promise`<[`Movie`](./<internal>.Movie)\>

#### Parameters

| Name | Type |
| :------ | :------ |
| `movieId` | `string` |

#### Returns

`Promise`<[`Movie`](./<internal>.Movie)\>

#### Defined in

[movie/service/index.ts:36](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/service/index.ts#L36)
