[<internal>](../modules/<internal>).Chapter

## Table of contents

### Constructors

- [constructor](./&lt;internal&gt;.Chapter#constructor)

### Properties

- [\_id](./&lt;internal&gt;.Chapter#_id)
- [bookId](./&lt;internal&gt;.Chapter#bookid)
- [name](./&lt;internal&gt;.Chapter#name)

### Methods

- [getBook](./&lt;internal&gt;.Chapter#getbook)

## Constructors

### constructor

• **new Chapter**(`__namedParameters`, `requestService`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `__namedParameters` | [`ChapterDoc`](../interfaces/<internal>.ChapterDoc) |
| `requestService` | [`DataService`](./<internal>.DataService) |

#### Defined in

[chapter/models/Chapter.ts:13](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/chapter/models/Chapter.ts#L13)

## Properties

### \_id

• **\_id**: `string`

#### Defined in

[chapter/models/Chapter.ts:7](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/chapter/models/Chapter.ts#L7)

___

### bookId

• **bookId**: `string`

#### Defined in

[chapter/models/Chapter.ts:9](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/chapter/models/Chapter.ts#L9)

___

### name

• **name**: `string`

#### Defined in

[chapter/models/Chapter.ts:8](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/chapter/models/Chapter.ts#L8)

## Methods

### getBook

▸ **getBook**(): `Promise`<[`Book`](./<internal>.Book)\>

#### Returns

`Promise`<[`Book`](./<internal>.Book)\>

#### Defined in

[chapter/models/Chapter.ts:23](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/chapter/models/Chapter.ts#L23)
