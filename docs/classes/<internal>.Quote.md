[<internal>](../modules/<internal>).Quote

## Table of contents

### Constructors

- [constructor](./&lt;internal&gt;.Quote#constructor)

### Properties

- [\_id](./&lt;internal&gt;.Quote#_id)
- [characterId](./&lt;internal&gt;.Quote#characterid)
- [dialog](./&lt;internal&gt;.Quote#dialog)
- [id](./&lt;internal&gt;.Quote#id)
- [movieId](./&lt;internal&gt;.Quote#movieid)

### Methods

- [getCharacter](./&lt;internal&gt;.Quote#getcharacter)
- [getMovie](./&lt;internal&gt;.Quote#getmovie)

## Constructors

### constructor

• **new Quote**(`doc`, `requestService`)

#### Parameters

| Name | Type |
| :------ | :------ |
| `doc` | [`QuoteDoc`](../interfaces/<internal>.QuoteDoc) |
| `requestService` | [`DataService`](./<internal>.DataService) |

#### Defined in

[quote/models/Quote.ts:20](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/models/Quote.ts#L20)

## Properties

### \_id

• **\_id**: `string`

#### Defined in

[quote/models/Quote.ts:9](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/models/Quote.ts#L9)

___

### characterId

• **characterId**: `string`

#### Defined in

[quote/models/Quote.ts:12](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/models/Quote.ts#L12)

___

### dialog

• **dialog**: `string`

#### Defined in

[quote/models/Quote.ts:10](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/models/Quote.ts#L10)

___

### id

• **id**: `string`

#### Defined in

[quote/models/Quote.ts:13](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/models/Quote.ts#L13)

___

### movieId

• **movieId**: `string`

#### Defined in

[quote/models/Quote.ts:11](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/models/Quote.ts#L11)

## Methods

### getCharacter

▸ **getCharacter**(): `Promise`<[`Character`](./<internal>.Character)\>

#### Returns

`Promise`<[`Character`](./<internal>.Character)\>

#### Defined in

[quote/models/Quote.ts:38](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/models/Quote.ts#L38)

___

### getMovie

▸ **getMovie**(): `Promise`<[`Movie`](./<internal>.Movie)\>

#### Returns

`Promise`<[`Movie`](./<internal>.Movie)\>

#### Defined in

[quote/models/Quote.ts:30](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/models/Quote.ts#L30)
