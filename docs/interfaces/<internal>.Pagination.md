[<internal>](../modules/<internal>).Pagination

## Table of contents

### Properties

- [limit](./&lt;internal&gt;.Pagination#limit)
- [offset](./&lt;internal&gt;.Pagination#offset)
- [page](./&lt;internal&gt;.Pagination#page)

## Properties

### limit

• `Optional` **limit**: `number`

#### Defined in

[data/models/Pagination.ts:2](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/Pagination.ts#L2)

___

### offset

• `Optional` **offset**: `number`

#### Defined in

[data/models/Pagination.ts:4](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/Pagination.ts#L4)

___

### page

• `Optional` **page**: `number`

#### Defined in

[data/models/Pagination.ts:3](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/Pagination.ts#L3)
