[<internal>](../modules/<internal>).QuoteDoc

## Table of contents

### Properties

- [\_id](./&lt;internal&gt;.QuoteDoc#_id)
- [character](./&lt;internal&gt;.QuoteDoc#character)
- [dialog](./&lt;internal&gt;.QuoteDoc#dialog)
- [id](./&lt;internal&gt;.QuoteDoc#id)
- [movie](./&lt;internal&gt;.QuoteDoc#movie)

## Properties

### \_id

• **\_id**: `string`

#### Defined in

[quote/models/QuoteDoc.ts:2](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/models/QuoteDoc.ts#L2)

___

### character

• **character**: `string`

#### Defined in

[quote/models/QuoteDoc.ts:5](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/models/QuoteDoc.ts#L5)

___

### dialog

• **dialog**: `string`

#### Defined in

[quote/models/QuoteDoc.ts:3](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/models/QuoteDoc.ts#L3)

___

### id

• **id**: `string`

#### Defined in

[quote/models/QuoteDoc.ts:6](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/models/QuoteDoc.ts#L6)

___

### movie

• **movie**: `string`

#### Defined in

[quote/models/QuoteDoc.ts:4](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/quote/models/QuoteDoc.ts#L4)
