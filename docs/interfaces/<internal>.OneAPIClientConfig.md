[<internal>](../modules/<internal>).OneAPIClientConfig

## Table of contents

### Properties

- [apiKey](./&lt;internal&gt;.OneAPIClientConfig#apikey)
- [requestService](./&lt;internal&gt;.OneAPIClientConfig#requestservice)

## Properties

### apiKey

• `Optional` **apiKey**: `string`

#### Defined in

[models/OneAPIClientConfig.ts:5](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/models/OneAPIClientConfig.ts#L5)

___

### requestService

• `Optional` **requestService**: [`DataService`](../classes/<internal>.DataService)

#### Defined in

[models/OneAPIClientConfig.ts:4](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/models/OneAPIClientConfig.ts#L4)
