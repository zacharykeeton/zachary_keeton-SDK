[<internal>](../modules/<internal>).MovieDoc

## Table of contents

### Properties

- [\_id](./&lt;internal&gt;.MovieDoc#_id)
- [academyAwardNominations](./&lt;internal&gt;.MovieDoc#academyawardnominations)
- [academyAwardWins](./&lt;internal&gt;.MovieDoc#academyawardwins)
- [boxOfficeRevenueInMillions](./&lt;internal&gt;.MovieDoc#boxofficerevenueinmillions)
- [budgetInMillions](./&lt;internal&gt;.MovieDoc#budgetinmillions)
- [name](./&lt;internal&gt;.MovieDoc#name)
- [rottenTomatoesScore](./&lt;internal&gt;.MovieDoc#rottentomatoesscore)
- [runtimeInMinutes](./&lt;internal&gt;.MovieDoc#runtimeinminutes)

## Properties

### \_id

• **\_id**: `string`

#### Defined in

[movie/models/MovieDoc.ts:2](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/models/MovieDoc.ts#L2)

___

### academyAwardNominations

• **academyAwardNominations**: `number`

#### Defined in

[movie/models/MovieDoc.ts:7](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/models/MovieDoc.ts#L7)

___

### academyAwardWins

• **academyAwardWins**: `number`

#### Defined in

[movie/models/MovieDoc.ts:8](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/models/MovieDoc.ts#L8)

___

### boxOfficeRevenueInMillions

• **boxOfficeRevenueInMillions**: `number`

#### Defined in

[movie/models/MovieDoc.ts:6](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/models/MovieDoc.ts#L6)

___

### budgetInMillions

• **budgetInMillions**: `number`

#### Defined in

[movie/models/MovieDoc.ts:5](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/models/MovieDoc.ts#L5)

___

### name

• **name**: `string`

#### Defined in

[movie/models/MovieDoc.ts:3](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/models/MovieDoc.ts#L3)

___

### rottenTomatoesScore

• **rottenTomatoesScore**: `number`

#### Defined in

[movie/models/MovieDoc.ts:9](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/models/MovieDoc.ts#L9)

___

### runtimeInMinutes

• **runtimeInMinutes**: `number`

#### Defined in

[movie/models/MovieDoc.ts:4](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/movie/models/MovieDoc.ts#L4)
