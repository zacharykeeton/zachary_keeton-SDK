[<internal>](../modules/<internal>).ServiceResult

## Hierarchy

- **`ServiceResult`**

  ↳ [`ChapterServiceResult`](./<internal>.ChapterServiceResult)

## Table of contents

### Properties

- [data](./&lt;internal&gt;.ServiceResult#data)
- [metadata](./&lt;internal&gt;.ServiceResult#metadata)

## Properties

### data

• **data**: `any`[]

#### Defined in

[models/ServiceResult.ts:2](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/models/ServiceResult.ts#L2)

___

### metadata

• **metadata**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `limit` | `number` |
| `offset` | `number` |
| `page` | `number` |
| `pages` | `number` |
| `total` | `number` |

#### Defined in

[models/ServiceResult.ts:3](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/models/ServiceResult.ts#L3)
