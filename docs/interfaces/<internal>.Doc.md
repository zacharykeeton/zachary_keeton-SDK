[<internal>](../modules/<internal>).Doc

## Hierarchy

- **`Doc`**

  ↳ [`ChapterDoc`](./<internal>.ChapterDoc)

## Table of contents

### Properties

- [\_id](./&lt;internal&gt;.Doc#_id)

## Properties

### \_id

• **\_id**: `string`

#### Defined in

[models/Doc.ts:2](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/models/Doc.ts#L2)
