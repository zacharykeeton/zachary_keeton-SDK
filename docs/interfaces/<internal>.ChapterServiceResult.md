[<internal>](../modules/<internal>).ChapterServiceResult

## Hierarchy

- [`ServiceResult`](./<internal>.ServiceResult)

  ↳ **`ChapterServiceResult`**

## Table of contents

### Properties

- [data](./&lt;internal&gt;.ChapterServiceResult#data)
- [metadata](./&lt;internal&gt;.ChapterServiceResult#metadata)

## Properties

### data

• **data**: [`Chapter`](../classes/<internal>.Chapter)[]

#### Overrides

[ServiceResult](./&lt;internal&gt;.ServiceResult).[data](./&lt;internal&gt;.ServiceResult#data)

#### Defined in

chapter/models/ChapterServiceResult.ts:5

___

### metadata

• **metadata**: `Object`

#### Type declaration

| Name | Type |
| :------ | :------ |
| `limit` | `number` |
| `offset` | `number` |
| `page` | `number` |
| `pages` | `number` |
| `total` | `number` |

#### Inherited from

[ServiceResult](./<internal>.ServiceResult).[metadata](./<internal>.ServiceResult#metadata)

#### Defined in

[models/ServiceResult.ts:3](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/models/ServiceResult.ts#L3)
