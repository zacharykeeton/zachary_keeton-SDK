[<internal>](../modules/<internal>).Sort

## Indexable

▪ [key: `string`]: [`SORT_DIRECTION`](../enums/SORT_DIRECTION)
