[<internal>](../modules/<internal>).Service

## Implemented by

- [`BookService`](../classes/<internal>.BookService)

## Table of contents

### Properties

- [find](./&lt;internal&gt;.Service#find)

## Properties

### find

• **find**: (`options?`: [`FindConfig`](./<internal>.FindConfig)) => `Promise`<[`ServiceResult`](./<internal>.ServiceResult)\>

#### Type declaration

▸ (`options?`): `Promise`<[`ServiceResult`](./<internal>.ServiceResult)\>

##### Parameters

| Name | Type |
| :------ | :------ |
| `options?` | [`FindConfig`](./<internal>.FindConfig) |

##### Returns

`Promise`<[`ServiceResult`](./<internal>.ServiceResult)\>

#### Defined in

[book/service/index.ts:10](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/book/service/index.ts#L10)
