[<internal>](../modules/<internal>).FindConfig

## Table of contents

### Properties

- [filter](./&lt;internal&gt;.FindConfig#filter)
- [pagination](./&lt;internal&gt;.FindConfig#pagination)
- [sort](./&lt;internal&gt;.FindConfig#sort)

## Properties

### filter

• `Optional` **filter**: [`Filter`](./<internal>.Filter)

#### Defined in

[data/models/FindConfig.ts:9](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/FindConfig.ts#L9)

___

### pagination

• `Optional` **pagination**: [`Pagination`](./<internal>.Pagination)

#### Defined in

[data/models/FindConfig.ts:7](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/FindConfig.ts#L7)

___

### sort

• `Optional` **sort**: [`Sort`](./<internal>.Sort)

#### Defined in

[data/models/FindConfig.ts:8](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/FindConfig.ts#L8)
