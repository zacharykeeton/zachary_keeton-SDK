[<internal>](../modules/<internal>).ChapterDoc

## Hierarchy

- [`Doc`](./<internal>.Doc)

  ↳ **`ChapterDoc`**

## Table of contents

### Properties

- [\_id](./&lt;internal&gt;.ChapterDoc#_id)
- [book](./&lt;internal&gt;.ChapterDoc#book)
- [chapterName](./&lt;internal&gt;.ChapterDoc#chaptername)

## Properties

### \_id

• **\_id**: `string`

#### Overrides

[Doc](./&lt;internal&gt;.Doc).[_id](./&lt;internal&gt;.Doc#_id)

#### Defined in

[chapter/models/ChapterDoc.ts:4](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/chapter/models/ChapterDoc.ts#L4)

___

### book

• **book**: `string`

#### Defined in

[chapter/models/ChapterDoc.ts:6](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/chapter/models/ChapterDoc.ts#L6)

___

### chapterName

• **chapterName**: `string`

#### Defined in

[chapter/models/ChapterDoc.ts:5](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/chapter/models/ChapterDoc.ts#L5)
