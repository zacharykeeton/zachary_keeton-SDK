[<internal>](../modules/<internal>).Filter

## Table of contents

### Properties

- [expression](./&lt;internal&gt;.Filter#expression)
- [key](./&lt;internal&gt;.Filter#key)
- [option](./&lt;internal&gt;.Filter#option)

## Properties

### expression

• `Optional` **expression**: `string` \| `RegExp` \| `string`[] \| `number`[]

#### Defined in

[data/models/Filter.ts:6](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/Filter.ts#L6)

___

### key

• **key**: `string`

#### Defined in

[data/models/Filter.ts:4](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/Filter.ts#L4)

___

### option

• **option**: [`FILTER_OPTIONS`](../enums/FILTER_OPTIONS)

#### Defined in

[data/models/Filter.ts:5](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/data/models/Filter.ts#L5)
