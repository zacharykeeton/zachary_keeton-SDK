[<internal>](../modules/<internal>).CharacterDoc

## Table of contents

### Properties

- [\_id](./&lt;internal&gt;.CharacterDoc#_id)
- [birth](./&lt;internal&gt;.CharacterDoc#birth)
- [death](./&lt;internal&gt;.CharacterDoc#death)
- [gender](./&lt;internal&gt;.CharacterDoc#gender)
- [hair](./&lt;internal&gt;.CharacterDoc#hair)
- [height](./&lt;internal&gt;.CharacterDoc#height)
- [name](./&lt;internal&gt;.CharacterDoc#name)
- [race](./&lt;internal&gt;.CharacterDoc#race)
- [realm](./&lt;internal&gt;.CharacterDoc#realm)
- [spouse](./&lt;internal&gt;.CharacterDoc#spouse)
- [wikiUrl](./&lt;internal&gt;.CharacterDoc#wikiurl)

## Properties

### \_id

• **\_id**: `string`

#### Defined in

[character/models/CharacterDoc.ts:2](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/CharacterDoc.ts#L2)

___

### birth

• **birth**: `string`

#### Defined in

[character/models/CharacterDoc.ts:6](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/CharacterDoc.ts#L6)

___

### death

• **death**: `string`

#### Defined in

[character/models/CharacterDoc.ts:8](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/CharacterDoc.ts#L8)

___

### gender

• **gender**: `string`

#### Defined in

[character/models/CharacterDoc.ts:5](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/CharacterDoc.ts#L5)

___

### hair

• **hair**: `string`

#### Defined in

[character/models/CharacterDoc.ts:10](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/CharacterDoc.ts#L10)

___

### height

• **height**: `string`

#### Defined in

[character/models/CharacterDoc.ts:3](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/CharacterDoc.ts#L3)

___

### name

• **name**: `string`

#### Defined in

[character/models/CharacterDoc.ts:11](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/CharacterDoc.ts#L11)

___

### race

• **race**: `string`

#### Defined in

[character/models/CharacterDoc.ts:4](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/CharacterDoc.ts#L4)

___

### realm

• **realm**: `string`

#### Defined in

[character/models/CharacterDoc.ts:9](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/CharacterDoc.ts#L9)

___

### spouse

• **spouse**: `string`

#### Defined in

[character/models/CharacterDoc.ts:7](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/CharacterDoc.ts#L7)

___

### wikiUrl

• **wikiUrl**: `string`

#### Defined in

[character/models/CharacterDoc.ts:12](https://gitlab.com/zacharykeeton/zachary_keeton-SDK/-/blob/a3ccbbd/src/character/models/CharacterDoc.ts#L12)
