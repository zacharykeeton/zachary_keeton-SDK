# the-one-sdk

A NodeJS sdk for https://the-one-api.dev/ written in typescript

## Installation

_NOTE_ requires node.js (v18+ recommended) & npm

```bash
npm install zachary_keeton-sdk
```

app.js

```javascript
// Import the class (commonjs)
const { OneAPIClient } = require("zachary_keeton-sdk");

// if using ES6 modules
// import { OneAPIClient } from 'zachary_keeton-sdk'

// Instantiate client
const client = new OneAPIClient({ apiKey: process.env.API_KEY });

// Make calls
client.books.find().then(console.log);
```

run

```bash
node app.js
```

## Sample App

see: https://gitlab.com/zacharykeeton/zachary_keeton-sdk-sample-app

prerequisite: nodejs on machine

```bash
git clone git@gitlab.com:zacharykeeton/zachary_keeton-sdk-sample-app.git
touch .env
# add single line to .env file: API_KEY="<yourAccessKey>"
npm install
npm start
```

## API

### Setup

note: You can get a free API key here: https://the-one-api.dev/sign-up

```javascript
const {
  OneAPIClient,
  SORT_DIRECTION, // used for sorting/filtering/pagination below
  FILTER_OPTIONS, // used for sorting/filtering/pagination below
} = require("zachary_keeton-sdk");

// Note: You need to get your API_KEY into the environment
// somehow e.g. .env files, docker container, etc
// Get a free API key here: https://the-one-api.dev/sign-up
const client = new OneAPIClient({ apiKey: process.env.API_KEY });
```

### Books

```javascript
// Find all books
client.books.find().then(console.log);

// Find books with pagination, sorting, filtering
const options = {
  pagination: {
    page: 2,
    limit: 1,
    offset: 1,
  },
  sort: { name: "desc" },
  filter: {
    key: "name",
    option: "NOT_MATCH_ANY",
    expression: "zach",
  },
};
client.books.find(options).then(console.log);

// Find a book by id
client.books.findOne("5cf58080b53e011a64671584").then(console.log);
```

### Movies

```javascript
// Find all movies
client.movies.find().then(console.log);

// Find movies with pagination, sorting, filtering
const options = {
  pagination: {
    page: 2,
    limit: 1,
    offset: 1,
  },
  sort: { name: "desc" },
  filter: {
    key: "name",
    option: "NOT_MATCH_ANY",
    expression: "zach",
  },
};
client.movies.find(options).then(console.log);

// Find a movie by id
client.movies.findOne("5cd95395de30eff6ebccde58").then(console.log);
```

### Quotes

```javascript
// Find all quotes
client.quotes.find().then(console.log);

// Find quotes with pagination, sorting, filtering
const options = {
  pagination: {
    page: 2,
    limit: 1,
    offset: 1,
  },
  sort: { name: "desc" },
  filter: {
    key: "name",
    option: "NOT_MATCH_ANY",
    expression: "zach",
  },
};
client.quotes.find(options).then(console.log);

// Find a quote by id
client.quotes.findOne("5cd96e05de30eff6ebcce7e9").then(console.log);

// Find a quotes by character id
client.quotes.findByCharacter("5cd99d4bde30eff6ebccfbe6").then(console.log);
```

### Chapters

```javascript
// Find all chapters
client.chapters.find().then(console.log);

// Find chapters with pagination, sorting, filtering
const options = {
  pagination: {
    page: 2,
    limit: 1,
    offset: 1,
  },
  sort: { name: "desc" },
  filter: {
    key: "name",
    option: "NOT_MATCH_ANY",
    expression: "zach",
  },
};
client.chapters.find(options).then(console.log);

// Find a chapter by id
client.chapters.findOne("6091b6d6d58360f988133b8d").then(console.log);

// Find a chapters by book id
client.chapters.findByBook("5cf58080b53e011a64671584").then(console.log);
```

### Characters

```javascript
// Find all characters
client.characters.find().then(console.log);

// Find characters with pagination, sorting, filtering
const options = {
  pagination: {
    page: 2,
    limit: 1,
    offset: 1,
  },
  sort: { name: "desc" },
  filter: {
    key: "name",
    option: "NOT_MATCH_ANY",
    expression: "zach",
  },
};
client.characters.find(options).then(console.log);

// Find a character by id
client.characters.findOne("5cd99d4bde30eff6ebccfbe6").then(console.log);
```

## Development

To develop, it is recommended to code with your test file watched like so:
note: be careful to respect the API rate limits if you're not going to
mock the http responses.

```bash
git clone
npm install
npm test -- --watch src/chapter/service/index.test.ts
```

You can also configure the `exec` value in `nodemon.json` to point to a working, sample app file and start it with:

```bash
npm run start:dev
```

Be sure to `.gitignore` it, etc.

## CICD

There is a CICD pipeline that will 1) run the e2e tests found in `src/index.test.ts`, and if they pass, will then 2) publish the npm package (note: be sure to increment the version number on pushes to main). No other protections on the branches, etc have been implemented. The NPM publish token and the API keys are secrets configures in gitlab (see: `settings/CICD/variables`)

## Known Shortcomings and Potential Improvements

- Code could be DRYer by leveraging generics
- Auto documentation generation with `typedoc` is incomplete (see: npm run gen_docs). It generates into the `docs` directory, but needs more attention (code comments => docs, etc.)
- Adding configurable retry logic to the dataService will make it more robust
- The querystring generator functions around pagination, sorting, and filtering could be refactored to be more elegant. Although they are passing their unit tests :)
- Add traversal functionality to the Classes (e.g Character.getQuotes()) to add more elegance to the API and improve developer experience.
- Add webpack to minify the deployed package for browser usage
